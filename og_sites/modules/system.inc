<?php

/**
 * Implementation of hook_og_sites_shared_tables().
 *
 * Return the array of core tables needed at all times.
 */
function system_og_sites_shared_tables() {
  return array(
    'sequences',
  );
}