<?php

/**
 * Implementation of hook_og_sites_menu().
 */
function node_og_sites_menu($may_cache) {
  global $user;

  $items = array();
  if (!$may_cache) {
    // Block access if this is not a local node.
    if (arg(0) == 'node' && is_numeric(arg(1)) && !og_sites_is_local_node(arg(1))) {
      $items[] = array(
        'path' => 'node/'. arg(1),
        'title' => t('View'),
        'type' => MENU_CALLBACK,
        'callback' => 'og_sites_not_found',
        'access' => TRUE,
      );
      $items[] = array(
        'path' => 'node/'. arg(1) .'/edit',
        'title' => t('Edit'),
        'type' => MENU_CALLBACK,
        'callback' => 'og_sites_not_found',
        'access' => TRUE,
      );
    }
  }
  return $items;
}

/**
 * Implementation of hook_og_sites_shared_tables().
 *
 * Return the array of tables shared by this module/component.
 */
function node_og_sites_shared_tables() {
  return array(
    'node',
    'node_revisions',
  );
}

/**
 * Implementation of hook_og_sites_form_alter().
 *
 * Alter forms to remove access to non-local nodes.
 */
function node_og_sites_form_alter($form_id, &$form) {
  switch ($form_id) {
    // Filter the nodes available for administration to show only local ones.
    case 'node_admin_nodes':
      // Empty the existing data.
      foreach (array('title', 'name', 'username', 'status', 'operations') as $key) {
        unset($form[$key]);
      }
      $filter = node_build_filter_query();

      // og_sites-specific code: add restrictions by site.
      $og_sites_sql = _og_sites_node_sql();
      $filter['join'] = !empty($filter['join']) ? $filter['join'] .' '. $og_sites_sql['join'] : $og_sites_sql['join'];
      $filter['where'] = !empty($filter['where']) ? $filter['where'] .' AND '. $og_sites_sql['where'] : ' WHERE '. $og_sites_sql['where'];

      $result = pager_query('SELECT n.*, u.name, u.uid FROM {node} n '. $filter['join'] .' INNER JOIN {users} u ON n.uid = u.uid '. $filter['where'] .' ORDER BY n.changed DESC', 50, 0, NULL, $filter['args']);
      $destination = drupal_get_destination();
      while ($node = db_fetch_object($result)) {
        $nodes[$node->nid] = '';
        $form['title'][$node->nid] = array('#value' => l($node->title, 'node/'. $node->nid) .' '. theme('mark', node_mark($node->nid, $node->changed)));
        $form['name'][$node->nid] =  array('#value' => node_get_types('name', $node));
        $form['username'][$node->nid] = array('#value' => theme('username', $node));
        $form['status'][$node->nid] =  array('#value' =>  ($node->status ? t('published') : t('not published')));
        $form['operations'][$node->nid] = array('#value' => l(t('edit'), 'node/'. $node->nid .'/edit', array(), $destination));
      }
      $form['nodes'] = array('#type' => 'checkboxes', '#options' => $nodes);
      $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
      break;
  }
}

/**
 * Implementation of hook_nodeapi().
 *
 * Handle node associations with this site's group on the hub site.
 */
function og_sites_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {

  switch ($op) {
    case 'delete':
      // No curly brackets on table name since we don't want the site's prefix.
      db_query('DELETE FROM '. OG_SITES_HUB_PREFIX .'og_ancestry WHERE nid = %d', $node->nid);
      break;
    case 'insert':
      // No curly brackets on table name since we don't want the site's prefix.
      db_query('INSERT INTO '. OG_SITES_HUB_PREFIX .'og_ancestry (nid, group_nid, is_public) VALUES (%d, %d, 1)', $node->nid, _og_sites_group_id());
      break;
  }
}

/**
 * Return the SQL fragments needed for filtering nodes to display only local ones.
 */
function _og_sites_node_sql($primary_table = 'n') {
  $return = array();
  // No curly brackets.
  $return['join'] = 'INNER JOIN '. OG_SITES_HUB_PREFIX .'og_ancestry hoa ON '. $primary_table .'.nid = hoa.nid INNER JOIN '. OG_SITES_HUB_PREFIX .'og_sites hos ON hoa.group_nid = hos.nid ';
  // We return nodes assigned to this site's group or the hub site's group.
  // We also return the node that is this site's group, so it can be edited locally.
  $return['where'] = "(hos.prefix = '". _og_sites_default_prefix() ."' OR hos.prefix = '". OG_SITES_HUB_PREFIX ."' OR ". $primary_table .".nid = hos.nid)";
  $return['distinct'] = 1;
  return $return;
}

/**
 * Determine if a node is local to the site.
 */
function og_sites_is_local_node($nid) {
  $og_sites_sql = _og_sites_node_sql();
  return db_num_rows(db_query('SELECT * FROM {node} n '. $og_sites_sql['join'] .' WHERE n.nid = %d AND '. $og_sites_sql['where'], $nid)) ? TRUE : FALSE;
}

/**
 * Implementation of hook_db_rewrite_sql().
 *
 * @todo: move this to og_sites, generalize with new hook?
 */
function og_sites_db_rewrite_sql($query, $primary_table, $primary_field) {
  if ($primary_field == 'nid') {
    return _og_sites_node_sql($primary_table);
  }
}
