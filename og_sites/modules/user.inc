<?php

/**
 * Implementation of hook_og_sites_menu().
 */
function user_og_sites_menu($may_cache) {
  global $user;

  $items = array();
  if (!$may_cache) {
    // Block access if this is not a local user.
    if (arg(0) == 'user' && is_numeric(arg(1)) && arg(1) > 0 && !og_sites_is_local_user(arg(1))) {
      $items[] = array(
        'path' => 'user/'. arg(1),
        'title' => t('User'),
        'type' => MENU_CALLBACK,
        'callback' => 'og_sites_not_found',
        'access' => TRUE,
      );
      $items[] = array(
        'path' => 'user/'. arg(1) .'/edit',
        'title' => t('Edit'),
        'type' => MENU_CALLBACK,
        'callback' => 'og_sites_not_found',
        'access' => TRUE,
      );
    }
  }
  return $items;
}

/**
 * Implementation of hook_og_sites_shared_tables().
 *
 * Return the array of tables shared by this module/component.
 */
function user_og_sites_shared_tables() {
  return array(
    'users',
    'node_revisions',
  );
}

/**
 * Implementation of hook_og_sites_form_alter().
 *
 * Alter forms to remove access to non-local users.
 */
function user_og_sites_form_alter($form_id, &$form) {
  switch ($form_id) {
    // Filter the users available for administration to show only local ones.
    case 'user_admin_account':
      // Empty the existing data.
      foreach (array('name', 'status', 'roles', 'member_for', 'last_access', 'operations') as $key) {
        unset($form[$key]);
      }
      $filter = user_build_filter_query();

      // og_sites-specific code: add restrictions by site.
      $og_sites_sql = _og_sites_user_sql();
      $filter['join'] = !empty($filter['join']) ? $filter['join'] .' '. $og_sites_sql['join'] : $og_sites_sql['join'];
      $filter['where'] = (!empty($filter['where']) ? $filter['where'] : '') . ' AND '. $og_sites_sql['where'];

      $header = array(
        array(),
        array('data' => t('Username'), 'field' => 'u.name'),
        array('data' => t('Status'), 'field' => 'u.status'),
        t('Roles'),
        array('data' => t('Member for'), 'field' => 'u.created', 'sort' => 'desc'),
        array('data' => t('Last access'), 'field' => 'u.access'),
        t('Operations')
      );

      $sql = 'SELECT DISTINCT u.uid, u.name, u.status, u.created, u.access FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid '. $filter['join'] .' WHERE u.uid != 0 '. $filter['where'];
      $sql .= tablesort_sql($header);
      $result = pager_query($sql, 50, 0, NULL, $filter['args']);
      while ($account = db_fetch_object($result)) {
        $accounts[$account->uid] = '';
        $form['name'][$account->uid] = array('#value' => theme('username', $account));
        $form['status'][$account->uid] =  array('#value' => $status[$account->status]);
        $users_roles = array();
        $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = %d', $account->uid);
        while ($user_role = db_fetch_object($roles_result)) {
          $users_roles[] = $roles[$user_role->rid];
        }
        asort($users_roles);
        $form['roles'][$account->uid][0] = array('#value' => theme('item_list', $users_roles));
        $form['member_for'][$account->uid] = array('#value' => format_interval(time() - $account->created));
        $form['last_access'][$account->uid] =  array('#value' => $account->access ? t('@time ago', array('@time' => format_interval(time() - $account->access))) : t('never'));
        $form['operations'][$account->uid] = array('#value' => l(t('edit'), "user/$account->uid/edit", array(), $destination));
      }
      $form['accounts'] = array(
        '#type' => 'checkboxes',
        '#options' => $accounts
      );
      $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
      break;
  }
}

/**
 * Implementation of hook_user().
 *
 * Handle user associations with this site's group node on the hub site.
 */
function og_sites_user($type, $edit, &$user, $category = NULL) {
  switch ($type) {
    case 'insert':
      og_sites_save_subscription(_og_sites_group_id(), $user->uid);
      break;
  }
}

/**
 * Determine if an item is local to the site.
 */
function og_sites_is_local_user($uid) {
  // No curly brackets on table name since we don't want the site's prefix.
  return db_num_rows(db_query('SELECT * FROM '. OG_SITES_HUB_PREFIX .'og_uid WHERE uid = %d AND nid = %d', $uid, _og_sites_group_id())) ? TRUE : FALSE;
}

/**
 * Return the SQL fragments needed for filtering users to display only local ones.
 */
function _og_sites_user_sql() {
  $return = array();
  // No curly brackets on table name since we don't want the site's prefix.
  $return['join'] = 'INNER JOIN '. OG_SITES_HUB_PREFIX .'og_uid hou ON u.uid = hou.uid INNER JOIN '. OG_SITES_HUB_PREFIX .'og_sites hos ON hou.nid = hos.nid';
  $return['where'] = "hos.prefix = '". _og_sites_default_prefix() ."'";
  return $return;
}
/**
 * Add a user to a group on the hub site.
 *
 * Adapted from og_save_subscription.
 *
 * @param $gid node ID of a group
 * @param $uid user ID of user
 * @param $args an array with details of this subscription. Possible array keys are:
     is_active, is_admin, mail_type, created
 */
function og_sites_save_subscription($gid, $uid, $args = array('is_active' => TRUE, 'is_admin' => FALSE)) {
  // No curly brackets.
  $sql = "SELECT COUNT(*) FROM ". OG_SITES_HUB_PREFIX ."og_uid WHERE nid = %d AND uid = %d";
  $cnt = db_result(db_query($sql, $gid, $uid));
  $time = time();
  if ($cnt == 0) {
    // this pattern borrowed from user_save()
    $fields = array('nid', 'uid', 'created', 'changed');
    $group = node_load($gid);
    $values = array($gid, $uid, $args['created'] ? $args['created'] : $time, $time);
    unset($args['created']);
    foreach ($args as $key => $value) {
      $fields[] = db_escape_string($key);
      $values[] = $value;
      $s[] = "'%s'";
    }
    // No curly brackets.
    db_query('INSERT INTO '. OG_SITES_HUB_PREFIX .'og_uid ('. implode(', ', $fields). ') VALUES (%d, %d, %d, %d, '. implode(', ', $s). ')', $values);
  }
  else {
    $cond[] = 'changed = '. $time;
    foreach ($args as $key => $value) {
      $cond[] = db_escape_string($key)." = '". db_escape_string($value). "'";
    }
    $cond = implode(', ', $cond);
    // No curly brackets.
    db_query("UPDATE ". OG_SITES_HUB_PREFIX ."og_uid SET $cond WHERE nid = %d AND uid = %d", $gid, $uid);
  }
}
