og_sites readme

NOTE: This module needs extensive testing before it's ready for use in a 
production environment.


OVERVIEW

Together, og_sites_hub and og_sites provide multisite functionality in which
a central 'hub' site has a set of associated sites leveraging Organic Groups
functionality to share data (nodes and users).

By default, Drupal supports table sharing between sites. In a multisite configuration, however, it is often desirable to selectively limit access 
between sites. For example, with simple table sharing, an admin user on one site could reset the password of user 1 on another site, gaining full control over that other site.

Control over access is especially needed in a configuration where one site
is a network hub, with many associated sites--e.g., an organization with a
head office and many branches. Under this configuration, the hub site needs
access to all users and content, but the branches should only have access
to their own content and users.

og_sites_hub runs on the hub site. It maintains links between users, groups,
and content. Each associated site is represented by an organic group on the
hub site.

og_sites runs on each client site. It registers all users and content with
the site's group on the hub site and restricts access to users and content
created on the client site or associated with the client site's group. There
is also a group created on the hub site that allows publishing of content
that will be visible on all associated (client) sites.

INSTALLATION AND SETUP

1. Install and enable og_sites_hub.module on the site that is to be the 
   hub site.

This site must use a database prefix. The default is 'hub_'. If you are not
using the default, you need to add the following line at the top of your hub
site's settings.php file, and at the top of every associate site's
settings.php file:

define('OG_SITES_HUB_PREFIX', 'myprefix_');

where 'myprefix_' is the prefix you're using for the hub site.

The install script does the following:

* Creates a new content type, Site (og_site).
* Makes this content type an organic groups type. This is the
  organic group type that represents sites in the newtork of
  associated sites. 
* Creates a new group record representing the hub site.
  (All content assigned to this group will be available
  on all associated sites.)

2. For each client site you're producing:

a. Install the new site into the same database as the hub site, giving it a
   prefix. (Install the site as usual using the Drupal installer.)

If you used a prefix other than 'hub_' on the hub site, define it at the top
of your new site's settings.php file:

define('OG_SITES_HUB_PREFIX', 'myprefix_');

where 'myprefix_' is the prefix you're using for the hub site.

Do minimal initial site configuration. The site name and site mission
you enter will be used when registering with the hub site, see step b.

b. Enable the og_sites module.

The install script will register the site with the hub site, creating an 
organic group record on the hub site.

c. Select which data types to share

Navigate to

  Administer > Organic groups > Organic groups sites

Here you have a list of the data types available for sharing.

d. On the hub site, create a new group for the site.

Under Create content, select the 'Site' content type. You will get a select
box for Site prefix. Select the prefix you gave to the new site.

That's it!

USING OG_SITES

Associated sites will work just as usual, except that behind the scenes data
exchanges are happening with the hub site.

Nodes

If node sharing is enabled in the module configuration, node data are shared.

Whenever a new node is created, it is added to the site's organic group on
the main site.

Access is limited to a subset of nodes:

* Those associated with the site (either created on the site or
  created on the hub site and added to the site's organic group).
* Those created on the hub site and added to the hub site's organic
  group.
* The site's organic group node on the hub site (so that a site can
  edit its own description on the hub site).

For example, site admins only see these nodes in node administration, and
both view and edit access are blocked to all other nodes.

Users

If user sharing is enabled in the module configuration, user data are shared.

Whenever a new user is created, it is subscribed to the site's organic group
on the main site.

All users from all sites can log onto this site. However, access is limited
to a subset of users:

* Those associated with the site (either created on the site or
  created on the hub site and added to the site's organic group).

For example, site admins only see these users in user administration, and
both view and edit access are blocked to all other users.

TECHNICAL NOTES

og_sites.module dynamically shares database tables. No changes are needed
to the site's settings.php file. Instead, og_sites does the sharing
based on the component options selected on the site.

View and edit access restrictions are implemented through:

* hook_db_rewrite_sql(), which puts an extra restriction on nodes
* hook_menu(), by overwriting existing menu items to items that
  should not be accessible
* hook_form_alter(), to reset editing pages (node and user administration)
  to exclude non-local items

EXTENDING OG_SITES

The module is built to be extensible through plugins. In the /modules
subdirectory of og_sites, each plugin file represents a module and associated
data types that can be shared. Currently, these are user and node.

Adding a new data type requires two components:

a. Organic groups support for assigning the data type to an organic group.

This exists in the core og.module for users and nodes. Additional data types
will need implementations in contrib modules. Some of these are already in
place. For example, og_vocabularies provides taxonomy support and 
og_block_visibility provides for block sharing.

b. Write a plugin.

See node.inc and user.inc for examples of what is involved.


KNOWN ISSUES/TODO

A main outstanding issue is node types. If a node type exists on the client
site but not on a the hub, viewing and editing of the node will be broken on
the hub site.

Potential solution: implement an og_node_type module (or, maybe, og_content,
based on CCK) and an associated plugin.
og_sites plugin. 


